/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "AboutScene.h"
#import "IntroScene.h"

@implementation AboutScene

-(id) initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0.82 green:0.226 blue:0.0 alpha:1];
        [self addBackButton];
    }
    return self;
}

-(void) addBackButton
{
    SKSpriteNode *img = [SKSpriteNode spriteNodeWithImageNamed:@"about.jpg"];
    img.position = CGPointMake(CGRectGetMidX(self.frame),
                               CGRectGetMidY(self.frame));
    img.xScale = 0.5;
    img.yScale = 0.5;
    [self addChild:img];
}

-(void) backToIntro
{
    IntroScene *s = [[IntroScene alloc] initWithSize:self.size];
    SKTransition *doors = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:s transition:doors];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInNode:self];
    
    float x = touchLocation.x;
    
    if (touchLocation.y > 550.0) {
        [self backToIntro];
    } else {
        if (x < 350) {
            // Encelade
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://encelade.info"]];
        } if (x < 650) {
            // Biou
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://vanschklift.com"]];
        } else {
            // Noliv
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://lesbubars.net"]];
        }
    }
    // Prio : si y > 550 -> Back
    // x: 0 à 350 pour Butch
    // 350 à 650 pour Biou
    // > 650 pour Noliv
    
}
@end
