//
//  BBAudioManager.h
//  inspiration PawAppsExample_SimpleAudioEngine
//
//  Created by Vincent on 27/01/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectAL.h"
#import "OALAudioTrack.h"

@interface BBAudioManager : NSObject  {
	
}

// preload all sfx and bgm files
-(void) preload;




// play an sfx file
-(void) playSFX:(NSString *)soundType;
// play a random sfx file in a list
-(void) playRandomSfx:(NSArray *) names;



+(BBAudioManager *) sharedAM;
@property (strong) NSMutableArray * sfxFiles;
@property (strong) NSMutableArray * bgmFiles;


@end
