//
//  BBAudioManager.m
//  inspiration PawAppsExample_SimpleAudioEngine
//
//  Created by Vincent on 27/01/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "BBAudioManager.h"

#ifdef __CC_PLATFORM_IOS
#endif
#import "BBGCDSingleton.h"


@implementation BBAudioManager

#pragma mark Singleton

static BBAudioManager *sharedAM = nil;


+ (BBAudioManager *) sharedAM
{
	DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
		return sharedAM = [[self alloc] init];
	});
}


#pragma mark AudioManager

-(id) init
{
	if( (self=[super init])) {
		// list all sound effects
		NSArray * caf = [[NSBundle mainBundle] pathsForResourcesOfType:@".caf" inDirectory:@"."];
		self.sfxFiles = [NSMutableArray arrayWithCapacity:[caf count]];
		for (NSString * s in caf) {
			//NSLog(@"%@", s);
			[self.sfxFiles addObject:[s lastPathComponent]];
		}
    }
   	return self;
}






-(void) playRandomSfx:(NSArray *) names {
	NSLog(@"playRandomSfx");
	int count = [names count];
	if (count != 0) {
		int r = (arc4random() % count);
		[self playSFX:[names objectAtIndex:r]];
	} else {
		NSLog(@"playRandomSfx: list is empty");
	}
}

// play
-(void) playSFX:(NSString *)soundType {
	NSLog(@"playSFX %@", soundType);
	if (![soundType isEqual:@""]) {
		if ([self.sfxFiles containsObject:soundType]) {
			NSLog(@"--play");
			[[OALSimpleAudio sharedInstance] playEffect:soundType];
		} else {
			NSLog(@"--Sfx not found:%@", soundType);
		}
	}
	
}

// preload files
-(void) preload {
	NSLog(@"preload");
	for (NSString* s in self.sfxFiles) {
		#ifdef __CC_PLATFORM_IOS
		[[OALSimpleAudio sharedInstance] preloadEffect:s];
		#endif
	}
}




@end
