/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "KKSpriteNode.h"
// haha !
#define kAnimatedSprite @"kPoue"
#define kMoveSprite @"plop"

typedef enum {
    PersonStateIdle = 0,
    PersonStateWalkUp,
    PersonStateWalkLeft,
    PersonStateWalkDown,
    PersonStateWalkRight,
    PersonStateWalkUpLeft,
    PersonStateWalkDownLeft,
    PersonStateWalkDownRight,
    PersonStateWalkUpRight
} PersonState;

@interface CharacterNode : KKSpriteNode {
@private
	NSMutableArray *spOpenSteps;
	NSMutableArray *spClosedSteps;
	NSMutableArray *shortestPath;
}

@property KKTilemapTileLayerNode * walls;
@property float pseudospeed;
@property NSMutableArray *frames;
@property KKSpriteNode *image;
@property PersonState state;

- (void)changeStateTo:(PersonState)state;
- (void)moveToward:(CGPoint)target;
- (void)drawRectangle;
- (void)moveLocalRandom;


@end
