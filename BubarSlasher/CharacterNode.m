/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "CharacterNode.h"
#import "BBAudioManager.h"

@interface CharacterNode ()

@property (nonatomic, retain) NSMutableArray *spOpenSteps;
@property (nonatomic, retain) NSMutableArray *spClosedSteps;
@property (nonatomic, retain) NSMutableArray *shortestPath;
@end

@interface ShortestPathStep : NSObject
{
	CGPoint position;
	int gScore;
	int hScore;
	__unsafe_unretained ShortestPathStep *parent;
}

@property (nonatomic, assign) CGPoint position;
@property (nonatomic, assign) int gScore;
@property (nonatomic, assign) int hScore;
@property (nonatomic, unsafe_unretained) ShortestPathStep *parent;

- (id)initWithPosition:(CGPoint)pos;
- (int)fScore;

@end


@implementation CharacterNode
@synthesize spOpenSteps;
@synthesize spClosedSteps;
@synthesize shortestPath;

- (id)init
{
    self = [super init];
    if (self) {
		self.shortestPath = nil;
		self.pseudospeed = 1.0f;
        self.state = PersonStateWalkUp;
		self.spOpenSteps = nil;
		self.spClosedSteps = nil;
    }
    return self;
}

-(void)changeStateTo:(PersonState)state
{
    if (self.state != state) {
        //NSLog(@"change state to");
        self.state = state;
        if (self.frames.count<state) {
            NSLog(@"Character - changeStateTo: No textures for state (add them to CharacterNode.frames)");
        } else {
            NSArray *textures = [self.frames objectAtIndex:state];
            
            if (textures.count == 0) {
                NSLog(@"Character - changeStateTo: No textures for state in self.frames[state]");
            } else if (textures.count == 1) {
                [self.image removeActionForKey:kAnimatedSprite];
                [self.image setTexture:[textures objectAtIndex:0]];
            } else {
                [self.image removeActionForKey:kAnimatedSprite];
                [self.image runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:textures timePerFrame:0.082f resize:NO restore:YES]] withKey:kAnimatedSprite];
                
                // Direction
                CGFloat multiplierForDirection;
                // Vers la gauche
                if (state == PersonStateWalkDownLeft ||
                    state == PersonStateWalkLeft ||
                    state == PersonStateWalkUpLeft) {
                    multiplierForDirection = -1;
                } else {
                    multiplierForDirection = 1;
                }
                self.image.xScale = fabs(self.image.xScale) * multiplierForDirection;
            }
        }
    }
}

-(void)moveLocalRandom
{
	CGPoint tileCoord = [_walls tileCoordForPoint:self.position];
	NSArray * adjacent = [self walkableAdjacentTilesCoordForTileCoord:tileCoord];

	uint32_t rnd = arc4random_uniform([adjacent count]);
	NSValue *randomObject = [adjacent objectAtIndex:rnd];
	CGPoint point = [randomObject CGPointValue];
    
    float cost = ((tileCoord.x != point.x) && (tileCoord.y != point.y)) ? 1.4f : 1.0f;
    
    [self runAction:[SKAction sequence:@[
                                         [SKAction runBlock:^{[self changeStateTo:[self newPersonStateWithCoords:[_walls positionForTileCoord:point]]];}],
                                         [SKAction moveTo:[_walls positionForTileCoord:point] duration:self.pseudospeed*cost],
                                         [SKAction runBlock:^{[self performSelector:@selector(moveLocalRandom) withObject:nil afterDelay:0.02f];}]
                                         ]] withKey:kMoveSprite];
    
}

- (void)moveToward:(CGPoint)target {
	// Get current tile coordinate and desired tile coord
	CGPoint fromTileCoord = [_walls tileCoordForPoint:self.position];
	CGPoint to = [_walls tileCoordForPoint:target];
	CGPoint toTileCoord = CGPointMake(to.x, to.y);
    // Check that there is a path to compute ;-)
    if (CGPointEqualToPoint(fromTileCoord, toTileCoord)) {
        NSLog(@"You're already there! :P");
        return;
    }
	
    // Must check that the desired location is walkable
    // In our case it's really easy, because only wall are unwalkable
    
	if ([self isWallAtTileCoord:toTileCoord]) {
		BBAudioManager * am = [BBAudioManager sharedAM];
		[am playRandomSfx:@[@"Mauvaise_Action_1.caf", @"Mauvaise_Action_2.caf"]];
		NSLog(@"y'a un mur");
        //[[SimpleAudioEngine sharedEngine] playEffect:@"hitWall.wav"];
        return;
    }
	self.spOpenSteps = [[NSMutableArray alloc] init];
	self.spClosedSteps = [[NSMutableArray alloc] init];
	
	// Start by adding the from position to the open list
	[self insertInOpenSteps:[[ShortestPathStep alloc] initWithPosition:fromTileCoord]];
	
	do {
		// Get the lowest F cost step
		// Because the list is ordered, the first step is always the one with the lowest F cost
		ShortestPathStep *currentStep = [self.spOpenSteps objectAtIndex:0];
		
		// Add the current step to the closed set
		[self.spClosedSteps addObject:currentStep];
		
		// Remove it from the open list
		// Note that if we wanted to first removing from the open list, care should be taken to the memory
		[self.spOpenSteps removeObjectAtIndex:0];
		
		// If the currentStep is the desired tile coordinate, we are done!
		if (CGPointEqualToPoint(currentStep.position, toTileCoord)) {
			
			[self constructPathAndStartAnimationFromStep:currentStep];
			self.spOpenSteps = nil; // Set to nil to release unused memory
			self.spClosedSteps = nil; // Set to nil to release unused memory
			break;
		}
		
		// Get the adjacent tiles coord of the current step
		NSArray *adjSteps = [self walkableAdjacentTilesCoordForTileCoord:currentStep.position];
		for (NSValue *v in adjSteps) {
			ShortestPathStep *step = [[ShortestPathStep alloc] initWithPosition:[v CGPointValue]];
			
			// Check if the step isn't already in the closed set
			if ([self.spClosedSteps containsObject:step]) {
				continue; // Ignore it
			}
			
			// Compute the cost from the current step to that step
			int moveCost = [self costToMoveFromStep:currentStep toAdjacentStep:step];
			
			// Check if the step is already in the open list
			NSUInteger index = [self.spOpenSteps indexOfObject:step];
			
			if (index == NSNotFound) { // Not on the open list, so add it
				
				// Set the current step as the parent
				step.parent = currentStep;
				
				// The G score is equal to the parent G score + the cost to move from the parent to it
				step.gScore = currentStep.gScore + moveCost;
				
				// Compute the H score which is the estimated movement cost to move from that step to the desired tile coordinate
				step.hScore = [self computeHScoreFromCoord:step.position toCoord:toTileCoord];
				
				// Adding it with the function which is preserving the list ordered by F score
				[self insertInOpenSteps:step];
				
			}
			else { // Already in the open list
				
				step = [self.spOpenSteps objectAtIndex:index]; // To retrieve the old one (which has its scores already computed ;-)
				
				// Check to see if the G score for that step is lower if we use the current step to get there
				if ((currentStep.gScore + moveCost) < step.gScore) {
					
					// The G score is equal to the parent G score + the cost to move from the parent to it
					step.gScore = currentStep.gScore + moveCost;
					
					// Because the G Score has changed, the F score may have changed too
					// So to keep the open list ordered we have to remove the step, and re-insert it with
					// the insert function which is preserving the list ordered by F score
					
					
					// Now we can removing it from the list without be afraid that it can be released
					[self.spOpenSteps removeObjectAtIndex:index];
					
					// Re-insert it with the function which is preserving the list ordered by F score
					[self insertInOpenSteps:step];
					
				}
			}
		}

	} while ([self.spOpenSteps count] > 0);
	

	if (self.shortestPath == nil) { // No path found
		//[[SimpleAudioEngine sharedEngine] playEffect:@"hitWall.wav"];
		NSLog(@"no path found, samere!");
	}
}

- (BOOL)isWallAtTileCoord:(CGPoint)coord
{
	if ([_walls.layer tileGidAt:coord] != 0) {
		return YES;
	} else {
		return NO;
	}
}

// Insert a path step (ShortestPathStep) in the ordered open steps list (spOpenSteps)
- (void)insertInOpenSteps:(ShortestPathStep *)step
{
	int stepFScore = [step fScore]; // Compute the step's F score
	int count = [self.spOpenSteps count];
	int i = 0; // This will be the index at which we will insert the step
	for (; i < count; i++) {
		if (stepFScore <= [[self.spOpenSteps objectAtIndex:i] fScore]) { // If the step's F score is lower or equals to the step at index i
			// Then we found the index at which we have to insert the new step
            // Basically we want the list sorted by F score
			break;
		}
	}
	// Insert the new step at the determined index to preserve the F score ordering
	[self.spOpenSteps insertObject:step atIndex:i];
}

// Compute the H score from a position to another (from the current position to the final desired position
- (int)computeHScoreFromCoord:(CGPoint)fromCoord toCoord:(CGPoint)toCoord
{
	// Here we use the Manhattan method, which calculates the total number of step moved horizontally and vertically to reach the
	// final desired step from the current step, ignoring any obstacles that may be in the way
	return abs(toCoord.x - fromCoord.x) + abs(toCoord.y - fromCoord.y);
}

// Compute the cost of moving from a step to an adjacent one
- (int)costToMoveFromStep:(ShortestPathStep *)fromStep toAdjacentStep:(ShortestPathStep *)toStep
{
    // Prise en compte mouvements diagonaux
	return ((fromStep.position.x != toStep.position.x) && (fromStep.position.y != toStep.position.y)) ? 14 : 10;
}


- (NSArray *)walkableAdjacentTilesCoordForTileCoord:(CGPoint)tileCoord
{
	NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:4];
	
    BOOL t = NO;
    BOOL l = NO;
    BOOL b = NO;
    BOOL r = NO;
    
	// Top
	CGPoint p = CGPointMake(tileCoord.x, tileCoord.y - 1);
	// FIXME normalement tester si les coordonnées sont valides (i.e. sur la map) mais normalement on renvoie YES pour les coords pas valides
	if (![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
        t=YES;
	}
	
	// Left
	p = CGPointMake(tileCoord.x - 1, tileCoord.y);
	if (![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
        l=YES;
	}
	
	// Bottom
	p = CGPointMake(tileCoord.x, tileCoord.y + 1);
	if (![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
        b=YES;
	}
	
	// Right
	p = CGPointMake(tileCoord.x + 1, tileCoord.y);
	if (![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
        r=YES;
	}
	
    
	// Top Left
	p = CGPointMake(tileCoord.x - 1, tileCoord.y - 1);
	if (t && l && ![self isWallAtTileCoord:p] && ![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
	}
    
	// Bottom Left
	p = CGPointMake(tileCoord.x - 1, tileCoord.y + 1);
	if (b && l && ![self isWallAtTileCoord:p] && ![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
	}
    
	// Top Right
	p = CGPointMake(tileCoord.x + 1, tileCoord.y - 1);
	if (t && r && ![self isWallAtTileCoord:p] && ![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
	}
    
	// Bottom Right
	p = CGPointMake(tileCoord.x + 1, tileCoord.y + 1);
	if (b && r && ![self isWallAtTileCoord:p] && ![self isWallAtTileCoord:p]) {
		[tmp addObject:[NSValue valueWithCGPoint:p]];
	}
 
	return [NSArray arrayWithArray:tmp];
}

// Go backward from a step (the final one) to reconstruct the shortest computed path
- (void)constructPathAndStartAnimationFromStep:(ShortestPathStep *)step
{
	self.shortestPath = [NSMutableArray array];
	
	do {
		if (step.parent != nil) { // Don't add the last step which is the start position (remember we go backward, so the last one is the origin position ;-)
			[self.shortestPath insertObject:step atIndex:0]; // Always insert at index 0 to reverse the path
		}
		step = step.parent; // Go backward
	} while (step != nil); // Until there is no more parents
	[self startAnimation];
}


-(void)startAnimation
{
	//NSLog(@"start animation");
	NSMutableArray * animSequence = [NSMutableArray array];
	//PersonState pstate = PersonStateIdle;
	//NSLog(@"state: idle");
	for (ShortestPathStep *s in self.shortestPath) {
		
        [animSequence addObject:[SKAction runBlock:^{
            [self changeStateTo:[self newPersonStateWithCoords:[_walls positionForTileCoord:s.position]]];
        }]];
        
		
    
		//NSLog(@"go %f %f", s.position.x, s.position.y);
		[animSequence addObject:[SKAction moveTo:[_walls positionForTileCoord:s.position] duration:self.pseudospeed]];
		//[animSequence addObject:[SKAction moveTo:[_walls positionForTileCoord:s.position] duration:[self newSpeed:[_walls positionForTileCoord:s.position]]]];
		//NSLog(@"--end round");
		
	}
	[animSequence addObject:[SKAction runBlock:^{ [self changeStateTo:PersonStateIdle]; }]];
	[self.shortestPath removeAllObjects];
	[self removeActionForKey:kMoveSprite];
	[self runAction:[KKAction sequence:animSequence] withKey:kMoveSprite];
}




-(PersonState)newPersonStateWithCoords:(CGPoint)newCoords
{
	//NSLog(@"%f %f %f %f", newCoords.x, newCoords.y, self.position.x, self.position.y);
	float diffx = newCoords.x - self.position.x;
	float diffy = newCoords.y - self.position.y;
	PersonState state;
	if (fabs(diffx) > fabs(diffy)) {
		if (diffx > 0.0f) {
			state = PersonStateWalkRight;
			//NSLog(@"right");
		} else {
			state = PersonStateWalkLeft;
			//NSLog(@"left");
		}
	} else {
		if (diffy > 0.0f) {
			state = PersonStateWalkUp;
			//NSLog(@"up");
		} else {
			state = PersonStateWalkDown;
			//NSLog(@"down");
		}
	}
	return state;
}

-(float)newSpeed:(CGPoint)newPos
{
	return sqrt(pow((newPos.x - self.position.x),2)+pow((newPos.y-self.position.y),2))/self.speed;
}


-(void)drawRectangle
{
    float maxx = self.size.width;
    float maxy = self.size.height;
    
    SKShapeNode* topLeft = [SKShapeNode node];
    UIBezierPath* topLeftBezierPath = [[UIBezierPath alloc] init];
    [topLeftBezierPath moveToPoint:CGPointMake(0.0, 0.0)];
    [topLeftBezierPath addLineToPoint:CGPointMake(0.0, maxy)];
    [topLeftBezierPath addLineToPoint:CGPointMake(maxx, maxy)];
    topLeft.path = topLeftBezierPath.CGPath;
    topLeft.lineWidth = 2.0;
    topLeft.strokeColor = [UIColor redColor];
    topLeft.antialiased = NO;
    [self addChild:topLeft];
    
    SKShapeNode* bottomRight = [SKShapeNode node];
    UIBezierPath* bottomRightBezierPath = [[UIBezierPath alloc] init];
    [bottomRightBezierPath moveToPoint:CGPointMake(0.0, 0.0)];
    [bottomRightBezierPath addLineToPoint:CGPointMake(maxx, 0.0)];
    [bottomRightBezierPath addLineToPoint:CGPointMake(maxx, maxy)];
    bottomRight.path = bottomRightBezierPath.CGPath;
    bottomRight.lineWidth = 2.0;
    bottomRight.strokeColor = [UIColor greenColor];
    bottomRight.antialiased = NO;
    [self addChild:bottomRight];
}

@end

@implementation ShortestPathStep

@synthesize position;
@synthesize gScore;
@synthesize hScore;
@synthesize parent;

- (id)initWithPosition:(CGPoint)pos
{
	if ((self = [super init])) {
		position = pos;
		gScore = 0;
		hScore = 0;
		parent = nil;
	}
	return self;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@  pos=[%.0f;%.0f]  g=%d  h=%d  f=%d", [super description], self.position.x, self.position.y, self.gScore, self.hScore, [self fScore]];
}

- (BOOL)isEqual:(ShortestPathStep *)other
{
	return CGPointEqualToPoint(self.position, other.position);
}

- (int)fScore
{
	return self.gScore + self.hScore;
}



@end
