/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "CopNode.h"

@implementation CopNode

- (id)init
{
    self = [super init];
    if (self) {
		self.poursuite = NO;
        self.frames = [NSMutableArray new];
        // Frames init for each possible PersonState
        SKTextureAtlas *heroAnimatedAtlas = [SKTextureAtlas atlasNamed:@"cop"];
        // idle
        NSMutableArray *idleArray = [NSMutableArray new];
        [idleArray addObject:[heroAnimatedAtlas textureNamed:@"copRight1.png"]];
        [self.frames addObject:idleArray];
        // top
        NSMutableArray *walkingUpArray = [NSMutableArray new];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:@"copUp1.png"]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:@"copUp2.png"]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:@"copUp3.png"]];
        [self.frames addObject:walkingUpArray];
        // left
        NSMutableArray *walkingLeftArray = [NSMutableArray new];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"copRight1.png"]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"copRight2.png"]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"copRight3.png"]];
        [self.frames addObject:walkingLeftArray];
        // bottom
        NSMutableArray *walkingDownArray = [NSMutableArray new];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"copDown1.png"]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"copDown2.png"]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"copDown3.png"]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"copDown4.png"]];
        [self.frames addObject:walkingDownArray];
        // walkingRight
        NSMutableArray *walkingRightArray = [NSMutableArray new];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"copRight1.png"]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"copRight2.png"]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"copRight3.png"]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"copRight4.png"]];
        [self.frames addObject:walkingRightArray];
        
        self.image = [KKSpriteNode spriteNodeWithImageNamed:@"copRight1"];
        self.image.size = CGSizeMake(self.image.size.width*0.5, self.image.size.height*0.5);
		self.size = self.image.size;
        [self addChild:self.image];
		
        
        self.image.position = CGPointMake(44.0f, +52.0f);
        
        self.state = PersonStateWalkUp;
        self.pseudospeed = 2.0f;
        [self changeStateTo:PersonStateIdle];
    }
    return self;
}

-(void)poursuiteVers:(CGPoint)position
{
    self.pseudospeed = 0.55f;
    [self removeActionForKey:kMoveSprite];
    [self moveToward:position];
    [self performSelector:@selector(stopPoursuite) withObject:nil afterDelay:2.5f];
	self.poursuite = YES;
}

- (void)stopPoursuite
{
	self.poursuite = NO;
    self.pseudospeed = 2.0;
    [self removeActionForKey:kMoveSprite];
    [self moveLocalRandom];
}

@end
