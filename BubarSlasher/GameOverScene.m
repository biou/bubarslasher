/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "GameOverScene.h"
#import "IntroScene.h"
#import "BBAudioManager.h"

@implementation GameOverScene


-(id) initWithSize:(CGSize)size andMessage:(NSString *)message
{
    self = [super initWithSize:size];
    if (self) {
	[OALSimpleAudio sharedInstance].backgroundTrack.gain = 0.0;
		BBAudioManager * am = [BBAudioManager sharedAM];
		[am playSFX:@"Game_Over.caf"];
        self.backgroundColor = [UIColor colorWithRed:0.82 green:0.226 blue:0.0 alpha:1];
		SKSpriteNode *img = [SKSpriteNode spriteNodeWithImageNamed:@"FondMenu"];
		img.position = CGPointMake(CGRectGetMidX(self.frame),
								   CGRectGetMidY(self.frame));
		[self addChild:img];
		SKLabelNode* titre = [SKLabelNode labelNodeWithFontNamed:@"Mark"];
		titre.text = @"Game Over";
		titre.fontSize = 80;
		titre.zPosition = 1;
		titre.position = CGPointMake(CGRectGetMidX(self.frame),
									 self.size.height-150);
		[self addChild:titre];
		SKLabelNode* label = [SKLabelNode labelNodeWithFontNamed:@"Mark"];
		label.text = message;
		label.fontSize = 100;
		label.zPosition = 1;
		label.position = CGPointMake(CGRectGetMidX(self.frame),
										   CGRectGetMidY(self.frame)*0.9 );
		[self addChild:label];
		
        [self addBackButton];
    }
    return self;
}

-(void) addBackButton
{
	SKLabelNode* buttonLabel = [SKLabelNode labelNodeWithFontNamed:@"Mark"];
	buttonLabel.text = @"Retour";
	buttonLabel.fontSize = 80;
	buttonLabel.zPosition = 1;
	buttonLabel.position = CGPointMake(CGRectGetMidX(self.frame),
									   self.size.height/2-300);
	[self addChild:buttonLabel];
    
    
	// KKButtonBehavior turns any node into a button
	KKButtonBehavior* buttonBehavior = [KKButtonBehavior behavior];
	buttonBehavior.selectedScale = 1.2;
    
	[buttonLabel addBehavior:buttonBehavior];
	
	// observe button execute notification
	[self observeNotification:KKButtonDidExecuteNotification
					 selector:@selector(playButtonDidExecute:)
					   object:buttonLabel];
    
}

-(void) playButtonDidExecute:(NSNotification*)notification
{
    IntroScene *s = [[IntroScene alloc] initWithSize:self.size];
    SKTransition *doors = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:s transition:doors];
}


@end
