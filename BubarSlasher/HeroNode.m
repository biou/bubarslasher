/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "HeroNode.h"

@implementation HeroNode


- (id)init
{
    self = [super init];
    if (self) {
        self.frames = [NSMutableArray new];		
        // Frames init for each possible PersonState
        SKTextureAtlas *heroAnimatedAtlas = [SKTextureAtlas atlasNamed:@"hero"];
        // idle
        NSMutableArray *idleArray = [NSMutableArray new];
        [idleArray addObject:[heroAnimatedAtlas textureNamed:@"idle.png"]];
        [self.frames addObject:idleArray];
        // top
        NSMutableArray *walkingUpArray = [NSMutableArray new];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:@"up1.png"]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:@"up2.png"]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:@"up3.png"]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:@"up4.png"]];
        [self.frames addObject:walkingUpArray];
        // left
        NSMutableArray *walkingLeftArray = [NSMutableArray new];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"droite01.png"]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"droite02.png"]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"droite03.png"]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"droite04.png"]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:@"droite05.png"]];
        [self.frames addObject:walkingLeftArray];
        // bottom
        NSMutableArray *walkingDownArray = [NSMutableArray new];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"down1.png"]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"down2.png"]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"down3.png"]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:@"down2.png"]];
        [self.frames addObject:walkingDownArray];
        // walkingRight
        NSMutableArray *walkingRightArray = [NSMutableArray new];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"droite01.png"]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"droite02.png"]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"droite03.png"]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"droite04.png"]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:@"droite05.png"]];
        [self.frames addObject:walkingRightArray];
        
        self.image = [KKSpriteNode spriteNodeWithImageNamed:@"idle.png"];
        self.image.size = CGSizeMake(self.image.size.width*0.5, self.image.size.height*0.5);
        [self addChild:self.image];
        self.image.position = CGPointMake(39.0f, +58.0f);
		self.size = self.image.size;
		self.pseudospeed = 0.4f;
        
        self.state = PersonStateWalkUp;
        
        [self changeStateTo:PersonStateIdle];
    }
    return self;
}


@end