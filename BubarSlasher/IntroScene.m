/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "IntroScene.h"
#import "MyScene.h"
#import "AboutScene.h"
#import "GameOverScene.h"
#import "BBAudioManager.h"

@implementation IntroScene

-(id) initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
		[[OALSimpleAudio sharedInstance]preloadBg:@"menu.aifc"];
		[OALSimpleAudio sharedInstance].backgroundTrack.gain = 0.8;

        self.backgroundColor = [UIColor colorWithRed:0.82 green:0.226 blue:0.0 alpha:1];
        [self addPlayButton];
		
		[[OALSimpleAudio sharedInstance] playBg:@"menu.aifc" loop:YES];
		BBAudioManager * am = [BBAudioManager sharedAM];
		[am preload];
    }
    return self;
}

-(void) addPlayButton
{
    SKSpriteNode *img = [SKSpriteNode spriteNodeWithImageNamed:@"Menu"];
    img.position = CGPointMake(CGRectGetMidX(self.frame),
                               CGRectGetMidY(self.frame));
    [self addChild:img];
    
	SKLabelNode* buttonLabel = [SKLabelNode labelNodeWithFontNamed:@"Mark"];
	buttonLabel.text = @"raser";
	buttonLabel.fontSize = 80;
	buttonLabel.zPosition = 1;
	buttonLabel.position = CGPointMake(CGRectGetMidX(self.frame)*1.61,
									   CGRectGetMidY(self.frame)*1.06);
	[self addChild:buttonLabel];
    
    
	// KKButtonBehavior turns any node into a button
	KKButtonBehavior* buttonBehavior = [KKButtonBehavior behavior];
	buttonBehavior.selectedScale = 1.2;
    
	[buttonLabel addBehavior:buttonBehavior];
	
	// observe button execute notification
	[self observeNotification:KKButtonDidExecuteNotification
					 selector:@selector(playButtonDidExecute:)
					   object:buttonLabel];
    
    
    SKLabelNode* buttonAbout = [SKLabelNode labelNodeWithFontNamed:@"Mark"];
	buttonAbout.text = @"credits";
	buttonAbout.fontSize = 70;
	buttonAbout.zPosition = 1;
	buttonAbout.position = CGPointMake(CGRectGetMidX(self.frame)*1.61,
									   CGRectGetMidY(self.frame)*0.68);
	[self addChild:buttonAbout];
	
    KKButtonBehavior* buttonBehavior2 = [KKButtonBehavior behavior];
	buttonBehavior2.selectedScale = 1.2;
	[buttonAbout addBehavior:buttonBehavior2];
	
	// observe button execute notification
	[self observeNotification:KKButtonDidExecuteNotification
					 selector:@selector(aboutButtonDidExecute:)
					   object:buttonAbout];
    
}

-(void) playButtonDidExecute:(NSNotification*)notification
{
	BBAudioManager * am = [BBAudioManager sharedAM];
	[am playSFX:@"Touche_Menu_2.caf"];
	[[OALSimpleAudio sharedInstance].backgroundTrack fadeTo:0 duration:0.5 target:self selector:@selector(loadgame)];
}

-(void)loadgame
{
	[OALSimpleAudio sharedInstance].backgroundTrack.gain = 0.8;
	MyScene *s = [[MyScene alloc] initWithSize:self.size];
    SKTransition *doors = [SKTransition doorsOpenVerticalWithDuration:0.5];
    [self.view presentScene:s transition:doors];
}

-(void) aboutButtonDidExecute:(NSNotification*)notification
{
	BBAudioManager * am = [BBAudioManager sharedAM];
	[am playSFX:@"Touche_Menu_2.caf"];
    AboutScene *s = [[AboutScene alloc] initWithSize:self.size];
    SKTransition *doors = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:s transition:doors];
}

@end
