/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "KKScene.h"

#define seuilBadPointDistance 32.0f

// IMPORTANT: in Kobold Kit all scenes must inherit from KKScene.
@interface MyScene : KKScene

@property BOOL rasageMode;
@property BOOL fakeRasageMode;
@property NSMutableArray *goodPathPoints;
@property NSMutableArray *badPathPoints;
@property int goodCount;
@property NSDate *rasageDate;
@property BOOL rasageTimeout;
@property BOOL perdu;
@property int rasagesReussis;
@property int rasagesFoires;
@end
