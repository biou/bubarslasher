/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Pathfinding algorithm largely inspired from 
 * http://www.raywenderlich.com/4970/how-to-implement-a-pathfinding-with-cocos2d-tutorial
 */

#import "MyScene.h"
#import "CharacterNode.h"
#import "HeroNode.h"
#import "PedestrianNode.h"
#import "CopNode.h"
#import "GameOverScene.h"
#import "YouWinScene.h"
#import "BBAudioManager.h"

@implementation MyScene

HeroNode * heroNode;
KKTilemapNode* tilemapNode;
KKTilemapLayerNode *backgroundLayerNode;
int mytime;
int bubarsToGo;

-(id) initWithSize:(CGSize)size
{
	self = [super initWithSize:size];
	if (self)
	{
		/* Setup your scene here */
        self.rasagesReussis = 0;
        self.rasagesFoires = 0;
		mytime = 90;
		bubarsToGo = 0;
		[OALSimpleAudio sharedInstance].backgroundTrack.gain = 0.8;
        
        self.view.ignoresSiblingOrder = YES;
		[[OALSimpleAudio sharedInstance]preloadBg:@"theme_main.aifc"];
        
		self.backgroundColor = [SKColor colorWithRed:0.4 green:0.0 blue:0.4 alpha:1.0];
		
        
		NSString* tmxFile = @"Tilemap.tmx";
        tilemapNode = [KKTilemapNode tilemapWithContentsOfFile:tmxFile];
        [self addChild:tilemapNode];
        
        backgroundLayerNode = [tilemapNode tileLayerNodeNamed:@"background"];
        KKTilemapLayerNode * fgLayerNode = [tilemapNode tileLayerNodeNamed:@"fg"];
        fgLayerNode.zPosition = 2.0f;
        KKTilemapLayer *fgLayer = fgLayerNode.layer;
        fgLayer.parallaxFactor = CGPointMake(1.0f, 1.0f);
        [tilemapNode enableParallaxScrolling];
        
        KKTilemapObject* heroObject = [tilemapNode objectNamed:@"hero"];
        CGPoint heroPosition = heroObject.position;
		
		KKTilemapTileLayerNode* tileLayerNode = [tilemapNode tileLayerNodeNamed:@"walls"];
		
		KKTilemapLayer* pdLayer = [tilemapNode.tilemap layerNamed:@"pedestrians"];

		for (KKTilemapObject *o in pdLayer.objects) {
			PedestrianNode * pdNode = [[PedestrianNode alloc] init];
			pdNode.position = o.position;
			pdNode.walls = tileLayerNode;
			pdNode.name = @"pedestrian";
            pdNode.xScale = 1.2f;
            pdNode.yScale = 1.2f;
			[backgroundLayerNode addChild:pdNode];
            [pdNode moveLocalRandom];
		}
		
		KKTilemapLayer* copLayer = [tilemapNode.tilemap layerNamed:@"cops"];
		
		for (KKTilemapObject *o in copLayer.objects) {
			CopNode * copNode = [[CopNode alloc] init];
			copNode.position = o.position;
			copNode.walls = tileLayerNode;
			copNode.name = @"cop";
			[backgroundLayerNode addChild:copNode];
			[copNode moveLocalRandom];
            copNode.xScale = 1.2f;
            copNode.yScale = 1.2f;
		}

		
        
        self.anchorPoint = CGPointMake(0.5, 0.5);
        
        heroNode = [[HeroNode alloc] init];
		heroNode.walls = tileLayerNode;
		heroNode.name = @"hero";
        heroNode.position = heroPosition;
        heroNode.zPosition = 1.0f;
        
        [tilemapNode.mainTileLayerNode addChild:heroNode];
        
        KKStayInBoundsBehavior *boundsLimit = [[KKStayInBoundsBehavior alloc] init];
        boundsLimit.bounds = tilemapNode.bounds;
        [heroNode addBehavior:boundsLimit];
        
        KKCameraFollowBehavior *cam = [[KKCameraFollowBehavior alloc] init];
        cam.scrollingNode = tilemapNode.mainTileLayerNode;
        
		
        [heroNode addBehavior:cam];
        
        
        // load good and bad rasage points
        NSString *path = [NSBundle pathForBundleFile:@"GoodPoints.plist"];
        self.goodPathPoints = [NSMutableArray arrayWithContentsOfFile:path];
        path = [NSBundle pathForBundleFile:@"BadPoints.plist"];
        self.badPathPoints = [NSMutableArray arrayWithContentsOfFile:path];
        
        // HUD
		bubarsToGo = [self countBubarsInGame];
		KKLabelNode *scoreHUD = [KKLabelNode labelNodeWithFontNamed:@"AvenirNext-Heavy"];
		scoreHUD.name = @"score";
		scoreHUD.fontColor = [UIColor whiteColor];
		scoreHUD.fontSize = 60;
		scoreHUD.position = CGPointMake(self.size.width/2-100, self.size.height/2-60);
		scoreHUD.zPosition = 8.0f;
		scoreHUD.text = [NSString stringWithFormat:@"0/%d", bubarsToGo ];
		[self addChild:scoreHUD];
		KKSpriteNode* bubarHead = [KKSpriteNode spriteNodeWithImageNamed:@"beardhead.png"];
        [self addChild:bubarHead];
        bubarHead.position = CGPointMake(self.size.width/2-200, self.size.height/2-38);
		bubarHead.zPosition = 8.0f;
		
		KKLabelNode *timeHUD = [KKLabelNode labelNodeWithFontNamed:@"AvenirNext-Heavy"];
		timeHUD.name = @"time";
		timeHUD.fontColor = [UIColor whiteColor];
		timeHUD.fontSize = 60;
		timeHUD.position = CGPointMake(-self.size.width/2+100, self.size.height/2-60);
		timeHUD.zPosition = 8.0f;
		timeHUD.text = [NSString stringWithFormat:@"⌚️ %d", mytime ];
		[self addChild:timeHUD];
		
		[self runAction:
			[SKAction repeatActionForever:
				[KKAction sequence:
					@[
					   [SKAction runBlock:^{ [self updateTime]; }],
					   [SKAction waitForDuration:1]
					]
				]
			]
		];
		
		[[OALSimpleAudio sharedInstance] playBg:@"theme_main.aifc" loop:YES];
	}
	return self;
}

-(int)countBubarsInGame
{
	__block int bubarsCountTotal = 0;
	[backgroundLayerNode enumerateChildNodesWithName:@"pedestrian" usingBlock:^(SKNode *node, BOOL *stop){
		PedestrianNode *pd = (PedestrianNode *)node;
		if (pd.pedestrianType != PedestrianPasBubar) {
			bubarsCountTotal++;
		}
	}];
	return bubarsCountTotal;
}

-(void) enterRasageMode
{
    //[self youwin];
	self.rasageDate = [NSDate new];
    self.rasageMode = YES;
    self.rasageTimeout = NO;
	backgroundLayerNode.speed = 0;
	tilemapNode.mainTileLayerNode.speed = 0;
	BBAudioManager * am = [BBAudioManager sharedAM];
	[am playSFX:@"Rasage_Malvenu_3.caf"];
    
    KKLabelNode *countdown = [KKLabelNode labelNodeWithFontNamed:@"AvenirNext-Heavy"];
    countdown.name = @"rasageCountdown";
    countdown.fontColor = [UIColor whiteColor];
    countdown.fontSize = 130;
    countdown.position = CGPointMake(0, CGRectGetMidY(self.view.bounds)*0.66f);
    countdown.zPosition = 8.0f;
    countdown.text = @" ";
    [self addChild:countdown];
    
    KKSpriteNode *faceDeCon = [KKSpriteNode spriteNodeWithImageNamed:@"Face.png"];
    faceDeCon.zPosition = 5.0f;
    faceDeCon.position = CGPointMake(0, 0);// CGPointMake(self.view.bounds.size.width*0.5f, self.view.bounds.size.height*0.5f);
    faceDeCon.name = @"BubarFace";
    faceDeCon.alpha=0.0;
    [self addChild:faceDeCon];
    
    KKSpriteNode *barbe = [KKSpriteNode spriteNodeWithImageNamed:@"Face-bubar-go.png"];
    barbe.zPosition = 6.0f;
    barbe.position = CGPointMake(0, 0); // CGPointMake(self.view.bounds.size.width*0.5f, self.view.bounds.size.height*0.5f);
    barbe.name = @"Barbe";
    barbe.alpha=0.0f;
    [self addChild:barbe];
    
    [faceDeCon runAction:[SKAction fadeAlphaTo:1.0f duration:0.35f]];
    [barbe runAction:[SKAction fadeAlphaTo:1.0f duration:0.35f]];
}

- (void)enterFakeRasageMode
{
    self.rasageTimeout = NO;
	self.fakeRasageMode = YES;
	backgroundLayerNode.speed = 0;
	tilemapNode.mainTileLayerNode.speed = 0;
	BBAudioManager * am = [BBAudioManager sharedAM];
	[am playRandomSfx:@[@"Rasage_Malvenu_1.caf", @"Rasage_Malvenu_2.caf"]];
    
    KKLabelNode *countdown = [KKLabelNode labelNodeWithFontNamed:@"AvenirNext-Heavy"];
    countdown.name = @"rasageCountdown";
    countdown.fontColor = [UIColor whiteColor];
    countdown.fontSize = 130;
    countdown.position = CGPointMake(0, CGRectGetMidY(self.view.bounds)*0.66f);
    countdown.zPosition = 8.0f;
    countdown.text = @"Pas un barbu !";
    [self addChild:countdown];
    
    KKSpriteNode *faceDeCon = [KKSpriteNode spriteNodeWithImageNamed:@"Face.png"];
    faceDeCon.zPosition = 5.0f;
    faceDeCon.position = CGPointMake(0, 0);// CGPointMake(self.view.bounds.size.width*0.5f, self.view.bounds.size.height*0.5f);
    faceDeCon.name = @"BubarFace";
    faceDeCon.alpha=0.0;
    [self addChild:faceDeCon];
    
    KKSpriteNode *barbe = [KKSpriteNode spriteNodeWithImageNamed:@"Face-blessure.png"];
    barbe.zPosition = 6.0f;
    barbe.position = CGPointMake(0, 0); // CGPointMake(self.view.bounds.size.width*0.5f, self.view.bounds.size.height*0.5f);
    barbe.name = @"Blessure";
    barbe.alpha=0.0f;
    [self addChild:barbe];
    
    [faceDeCon runAction:[SKAction fadeAlphaTo:1.0f duration:0.35f]];
    [barbe runAction:[SKAction fadeAlphaTo:1.0f duration:0.35f]];
    
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:0.35f],[SKAction runBlock:^{[self leaveRasageMode];}]]]];
}


-(void) clearSpaceButtonDidExecute:(NSNotification*)notification
{
	[[OALSimpleAudio sharedInstance] playEffect:@"die.wav"];
    
	[self enumerateChildNodesWithName:@"spaceship" usingBlock:^(SKNode* node, BOOL* stop) {
		// enable physics, makes spaceships drop (they will be removed by the custom behavior)
		CGFloat radius = node.frame.size.width / 4.0;
		node.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:radius];
	}];
}



#if TARGET_OS_IPHONE // iOS


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	//UITouch *touch = [touches anyObject];
	//CGPoint touchLocation = [touch locationInNode:self];
	// (optional) call super implementation to allow KKScene to dispatch touch events
	//[super touchesBegan:touches withEvent:event];
    
    self.goodCount = 0;
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInNode:self];
    
    if (self.rasageMode) {
        // pour chaque point de good et bad points, mesurer la distance et loguer la plus courte
        float closestGood = 999999;
        for (NSDictionary *dico in self.goodPathPoints) {
            NSNumber *xxx = [dico valueForKey:@"x"];
            NSNumber *yyy = [dico valueForKey:@"y"];
            CGPoint point = CGPointMake([xxx floatValue], [yyy floatValue]);
            float distance = [self distanceFrom:point to:touchLocation];
            if (distance < closestGood) {
                closestGood = distance;
            }
        }
        
        if (closestGood < seuilBadPointDistance*0.8f) {
            self.goodCount++;
        }
        
        float closestBad = 999999;
        for (NSDictionary *dico in self.badPathPoints) {
            NSNumber *xxx = [dico valueForKey:@"x"];
            NSNumber *yyy = [dico valueForKey:@"y"];
            CGPoint point = CGPointMake([xxx floatValue], [yyy floatValue]);
            float distance = [self distanceFrom:point to:touchLocation];
            if (distance < closestBad) {
                closestBad = distance;
            }
        }
        
        if (closestBad < seuilBadPointDistance) {
            NSLog(@"LOOOOOOOOOSEEEEEUUUUUUUUURRRR");
            NSLog(@"LOOOOOOOOOSEEEEEUUUUUUUUURRRR");
            self.goodCount = -1000;
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInNode:self];

    if (self.rasageMode) {
        NSLog(@"Good Count : %d",self.goodCount);
        if (self.goodCount > 5) {
            // Win Rasage
            SKNode *barbe = [self childNodeWithName:@"Barbe"];
            CGPoint poposition = barbe.position;
            [barbe removeFromParent];
            KKSpriteNode *surpris = [KKSpriteNode spriteNodeWithImageNamed:@"Face-surpris.png"];
            surpris.zPosition = 6.0f;
            surpris.position = poposition;
            surpris.name = @"Surpris";
            [self addChild:surpris];
        } else {
            // Lose Rasage
            SKNode *barbe = [self childNodeWithName:@"Barbe"];
            CGPoint poposition = barbe.position;
            [barbe removeFromParent];
            KKSpriteNode *blessure = [KKSpriteNode spriteNodeWithImageNamed:@"Face-blessure.png"];
            blessure.zPosition = 6.0f;
            blessure.position = poposition;
            blessure.name = @"Blessure";
            [self addChild:blessure];
        }
        [self performSelector:@selector(leaveRasageMode) withObject:nil afterDelay:0.6f];
    } else {
        CGPoint newCoords = CGPointMake(touchLocation.x - tilemapNode.mainTileLayerNode.position.x, touchLocation.y - tilemapNode.mainTileLayerNode.position.y);
		//NSLog(@"touch %@", NSStringFromCGPoint(newCoords));
        //[heroNode runAction:actionMove withKey:kAnimatedSprite];
        [heroNode moveToward:newCoords];
    }
}

-(void)leaveRasageMode
{
    
	if (!self.rasageMode && !self.fakeRasageMode) {
		return;
	}
	self.rasageMode = NO;
	self.fakeRasageMode = NO;
    SKNode *face = [self childNodeWithName:@"BubarFace"];
    SKNode *surpris = [self childNodeWithName:@"Surpris"];
    if (surpris) {
        self.rasagesReussis++;
		SKNode* score = [self childNodeWithName:@"score"];
		((SKLabelNode *)score).text = [NSString stringWithFormat:@"%d/%d", self.rasagesReussis, bubarsToGo ];
		
        [face runAction:[SKAction sequence:@[[SKAction fadeAlphaTo:0.0f duration:0.6f],[SKAction runBlock:^{[face removeFromParent];}]]]];
        [surpris runAction:[SKAction sequence:@[[SKAction fadeAlphaTo:0.0f duration:0.6f],[SKAction runBlock:^{[surpris removeFromParent];}]]]];
        SKNode *label = [self childNodeWithName:@"rasageCountdown"];
        ((SKLabelNode *)label).text = @"Bravo !";
		BBAudioManager * am = [BBAudioManager sharedAM];
		[am playRandomSfx:@[@"Rasage_reussi_1.caf", @"Rasage_reussi_2.caf"]];
        [label runAction:[SKAction sequence:@[[SKAction waitForDuration:0.25f],[SKAction fadeAlphaTo:0.0f duration:0.5f],[SKAction runBlock:^{[label removeFromParent];}]]]];
    }
    SKNode *blessure = [self childNodeWithName:@"Blessure"];
    if (blessure) {
        [face runAction:[SKAction sequence:@[[SKAction waitForDuration:0.3f],[SKAction fadeAlphaTo:0.0f duration:1.0f],[SKAction runBlock:^{[face removeFromParent];}]]]];
        [blessure runAction:[SKAction sequence:@[[SKAction waitForDuration:0.3f],[SKAction fadeAlphaTo:0.0f duration:1.0f],[SKAction runBlock:^{[blessure removeFromParent];}]]]];
        SKNode *label = [self childNodeWithName:@"rasageCountdown"];
        ((SKLabelNode *)label).text = @"Attention !";
		BBAudioManager * am = [BBAudioManager sharedAM];
		[am playRandomSfx:@[@"Rasage_rate_1.caf", @"Rasage_rate_2.caf"]];
        [label runAction:[SKAction sequence:@[[SKAction waitForDuration:0.3f],[SKAction fadeAlphaTo:0.0f duration:1.0f],[SKAction runBlock:^{[label removeFromParent];}]]]];
    }
    
    if (self.rasageTimeout) {
        SKNode *label = [self childNodeWithName:@"rasageCountdown"];
        
        SKNode *barbe = [self childNodeWithName:@"Barbe"];
        [barbe runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"Face-bubar-timeout.png"]]];
        [face runAction:[SKAction sequence:@[[SKAction waitForDuration:0.5f],[SKAction fadeAlphaTo:0.0f duration:0.4f],[SKAction runBlock:^{[face removeFromParent];}]]]];
        [barbe runAction:[SKAction sequence:@[[SKAction waitForDuration:0.5f],[SKAction fadeAlphaTo:0.0f duration:0.4f],[SKAction runBlock:^{[barbe removeFromParent];}]]]];
        [label runAction:[SKAction sequence:@[[SKAction waitForDuration:0.5f],[SKAction fadeAlphaTo:0.0f duration:0.4f],[SKAction runBlock:^{[label removeFromParent];}]]]];
        
        self.rasageTimeout = NO;
    }
    
    if (!surpris) {
        self.rasagesFoires++;
        [self poursuite];
    }
	
	// plus de bubar en jeu
	if ([self countBubarsInGame] == 0) {
		if (self.rasagesReussis == bubarsToGo) {
			[self youwin];
		} else {
			[self gameover:@"Echec"];
		}
	}
	backgroundLayerNode.speed = 1;
	tilemapNode.mainTileLayerNode.speed = 1;
}

#else // Mac OS X
-(void) mouseDown:(NSEvent *)event
{
	/* Called when a mouse click occurs */
	
	CGPoint location = [event locationInNode:self];
    
	// (optional) call super implementation to allow KKScene to dispatch mouse events
	[super mouseDown:event];
}
#endif

-(void) update:(CFTimeInterval)currentTime
{
    if (self.perdu) {
//        GameOverScene *s = [[GameOverScene alloc] initWithSize:self.size];
//        SKTransition *doors = [SKTransition fadeWithDuration:0.5];
//        [self.view presentScene:s transition:doors];
    }
    
	/* Called before each frame is rendered */
	
	// (optional) call super implementation to allow KKScene to dispatch update events
	[super update:currentTime];
    
    if (self.rasageMode) {
        NSTimeInterval dt = [self.rasageDate timeIntervalSinceNow];
        dt *= -10;
        dt = floorf(dt);
        if (dt > 3) {
            SKNode *nene = [self childNodeWithName:@"rasageCountdown"];
            if (nene) {
                SKLabelNode *label = (SKLabelNode *) nene;
                label.text = [NSString stringWithFormat:@"%1.0f",20-dt];
                
                if (dt>20) {
                    label.text = @"Trop tard !";
                }
            }
            
            if (dt > 20) {
                self.rasageTimeout = YES;
                [self leaveRasageMode];
            }
        }
    }
}


// Utilisée pour sauvegarger les points
-(void)savePointsWithTouchLocation:(CGPoint)touchLocation
{
    if (!self.goodPathPoints) {
        self.goodPathPoints = [NSMutableArray new];
    }
    
    if (self.goodPathPoints.count<8) {
        NSMutableDictionary *dico = [NSMutableDictionary new];
        [dico setObject:[NSNumber numberWithFloat:touchLocation.x] forKey:@"x"];
        [dico setObject:[NSNumber numberWithFloat:touchLocation.y] forKey:@"y"];
        [self.goodPathPoints addObject:dico];
    } else if (self.goodPathPoints.count == 8) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
        NSString *path = [paths objectAtIndex:0];
        NSString *filepath = [path stringByAppendingPathComponent:@"GoodPointsSaved.plist"];
        [self.goodPathPoints writeToFile:filepath atomically:YES];
        [self.goodPathPoints addObject:@"hahaha"];
        NSLog(@"SAVED GOOD POINTS HAHAHA");
    } else {
        if (!self.badPathPoints) {
            self.badPathPoints = [NSMutableArray new];
        }
        if (self.badPathPoints.count<16) {
            NSMutableDictionary *dico = [NSMutableDictionary new];
            [dico setObject:[NSNumber numberWithFloat:touchLocation.x] forKey:@"x"];
            [dico setObject:[NSNumber numberWithFloat:touchLocation.y] forKey:@"y"];
            [self.badPathPoints addObject:dico];
        } else if (self.badPathPoints.count == 16) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
            NSString *path = [paths objectAtIndex:0];
            NSString *filepath = [path stringByAppendingPathComponent:@"BadPointsSaved.plist"];
            [self.badPathPoints writeToFile:filepath atomically:YES];
            [self.badPathPoints addObject:@"hahaha"];
            NSLog(@"SAVED BAD POINTS HAHAHA");
        }
    }

}

-(float)distanceFrom:(CGPoint)point1 to:(CGPoint)point2
{
    CGFloat xDist = (point2.x - point1.x);
    CGFloat yDist = (point2.y - point1.y);
    return sqrt((xDist * xDist) + (yDist * yDist));
}

- (void)checkCollisions {
	// check collisions avec les piétons
	[backgroundLayerNode enumerateChildNodesWithName:@"pedestrian" usingBlock:^(SKNode *node, BOOL *stop){
		SKSpriteNode *pd = (SKSpriteNode *)node;
		CGRect smallerFrame = CGRectInset(pd.frame, 20, 20);
        CGRect heroSmallerFrame = CGRectInset(heroNode.frame, 20, 20);

		if (CGRectIntersectsRect(smallerFrame,heroSmallerFrame)) {
			NSLog(@"collision!!");
            PedestrianNode *pieton = (PedestrianNode *)pd;
            if (pieton.pedestrianType != PedestrianPasBubar) {
                [self enterRasageMode];
            } else {
                [self enterFakeRasageMode];
            }
            
            //[self poursuite];
			[pd removeFromParent];
		}
	}];
	
	// check collision avec les flics
	[backgroundLayerNode enumerateChildNodesWithName:@"cop" usingBlock:^(SKNode *node, BOOL *stop){
		CopNode *cop = (CopNode *)node;
		if (cop.poursuite) {
			CGRect smallerFrame = CGRectInset(cop.frame, 20, 20);
			if (CGRectIntersectsRect(smallerFrame,heroNode.frame)) {
				NSLog(@"perdu!!");
				[self gameover:@"Prison"];
			}
		}
	}];
}

-(void)gameover:(NSString *) message {
	self.perdu = YES;
	GameOverScene *s = [[GameOverScene alloc] initWithSize:self.size andMessage:message];
	[self.view presentScene:s];
}

-(void)youwin {
	self.perdu = NO;
	YouWinScene *s = [[YouWinScene alloc] initWithSize:self.size];
	[self.view presentScene:s];
}

-(void)poursuite {
	BBAudioManager * am = [BBAudioManager sharedAM];
	[am playRandomSfx:@[@"Flics_1.caf", @"Flics_2.caf"]];
		[self performSelector:@selector(suivreHero) withObject:Nil afterDelay:1.0];
		[self performSelector:@selector(suivreHero) withObject:Nil afterDelay:4.0];
}

-(void)suivreHero {
	__block CopNode * nearestCop = Nil;
	__block float distance = 100000.0;
	[backgroundLayerNode enumerateChildNodesWithName:@"cop" usingBlock:^(SKNode *node, BOOL *stop){
		CopNode *cop = (CopNode *)node;
		float newdist = sqrt(pow(heroNode.position.x - cop.position.x, 2)+ pow(heroNode.position.y - cop.position.y, 2));
		if (newdist< distance) {
			distance = newdist;
			nearestCop = cop;
		}
	}];
	[nearestCop poursuiteVers:heroNode.position];
}

-(void)updateTime {
	mytime--;
	if (mytime < 0) {
		[self gameover:@"Temps ecoule"];
	} else {
		SKNode* time = [self childNodeWithName:@"time"];
		((SKLabelNode *)time).text = [NSString stringWithFormat:@"⌚️ %d", mytime ];
	}
}


- (void)didEvaluateActions {
	[super didEvaluateActions];
	if (!self.rasageMode) {
        [self checkCollisions];
    }
}

@end
