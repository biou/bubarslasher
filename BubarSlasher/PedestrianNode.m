/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "PedestrianNode.h"

@implementation PedestrianNode

- (id)init
{
    self = [super init];
    if (self) {
        int typeMecInt = arc4random()%3;
        PedestrianType typeMec;
        switch (typeMecInt) {
            case 1:
                typeMec = PedestrianBubar;
                break;
            case 2:
                typeMec = PedestrianMaxiBubar;
                break;
            default:
                typeMec = PedestrianPasBubar;
                break;
        }
        return [self initWithType:typeMec];
    }
    return self;
}

- (id)initWithType:(PedestrianType)type
{
    self = [super init];
    if (self) {
        self.pedestrianType = type;
        self.frames = [NSMutableArray new];
        
        // Frames init for each possible PersonState
        SKTextureAtlas *heroAnimatedAtlas = [SKTextureAtlas atlasNamed:@"mec"];
        
        NSString *prefix;
        
        switch (type) {
            case PedestrianPasBubar:
                prefix = @"a";
                break;
            case PedestrianBubar:
                prefix = @"b";
                break;
            case PedestrianMaxiBubar:
                prefix = @"c";
                break;
            default:
                break;
        }
        
        // idle
        NSMutableArray *idleArray = [NSMutableArray new];
        [idleArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,1]]];
        [self.frames addObject:idleArray];
        // top
        NSMutableArray *walkingUpArray = [NSMutableArray new];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,1]]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,3]]];
        [walkingUpArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [self.frames addObject:walkingUpArray];
        // left
        NSMutableArray *walkingLeftArray = [NSMutableArray new];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,1]]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,3]]];
        [walkingLeftArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [self.frames addObject:walkingLeftArray];
        // bottom
        NSMutableArray *walkingDownArray = [NSMutableArray new];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,1]]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,3]]];
        [walkingDownArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [self.frames addObject:walkingDownArray];
        // walkingRight
        NSMutableArray *walkingRightArray = [NSMutableArray new];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,1]]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,3]]];
        [walkingRightArray addObject:[heroAnimatedAtlas textureNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,2]]];
        [self.frames addObject:walkingRightArray];
        
        self.image = [KKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"%@-droite%d",prefix,1]];
        self.image.size = CGSizeMake(self.image.size.width*0.5, self.image.size.height*0.5);
		self.size = self.image.size;
        [self addChild:self.image];
        
        self.image.position = CGPointMake(44.0f, +52.0f);
        
        self.state = PersonStateWalkUp;
        self.pseudospeed = 2.0f;
        [self changeStateTo:PersonStateIdle];
    }
    return self;
}

@end
