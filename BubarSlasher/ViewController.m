/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import <KoboldKit.h>
#import "ViewController.h"
#import "MyScene.h"
#import "IntroScene.h"

@implementation ViewController

-(void) presentFirstScene
{
	NSLog(@"%@", koboldKitCommunityVersion());
	NSLog(@"%@", koboldKitProVersion());

	// create and present first scene
	IntroScene* myScene = [IntroScene sceneWithSize:self.view.bounds.size];
	[self.kkView presentScene:myScene];
}

@end
