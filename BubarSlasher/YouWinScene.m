/*
 * Copyright (c) 2014 Les Bubars & Encelade
 * Released under the GPL v2 License:
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

#import "YouWinScene.h"
#import "IntroScene.h"
#import "BBAudioManager.h"

@implementation YouWinScene


-(id) initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
	[OALSimpleAudio sharedInstance].backgroundTrack.gain = 0.0;	
		BBAudioManager * am = [BBAudioManager sharedAM];
		[am playSFX:@"Gagne.caf"];
        self.backgroundColor = [UIColor colorWithRed:0.82 green:0.226 blue:0.0 alpha:1];
		SKSpriteNode *img = [SKSpriteNode spriteNodeWithImageNamed:@"FondMenu"];
		img.position = CGPointMake(CGRectGetMidX(self.frame),
								   CGRectGetMidY(self.frame));
		[self addChild:img];
		SKLabelNode* titre = [SKLabelNode labelNodeWithFontNamed:@"Mark"];
		titre.text = @"Victoire";
		titre.fontSize = 150;
		titre.zPosition = 1;
		titre.position = CGPointMake(CGRectGetMidX(self.frame),
									 self.size.height/2);
		[self addChild:titre];
        [self addBackButton];
    }
    return self;
}

-(void) addBackButton
{
	SKLabelNode* buttonLabel = [SKLabelNode labelNodeWithFontNamed:@"Mark"];
	buttonLabel.text = @"Retour";
	buttonLabel.fontSize = 80;
	buttonLabel.zPosition = 1;
	buttonLabel.position = CGPointMake(CGRectGetMidX(self.frame),
									   self.size.height/2-300);
	[self addChild:buttonLabel];
    
    
	// KKButtonBehavior turns any node into a button
	KKButtonBehavior* buttonBehavior = [KKButtonBehavior behavior];
	buttonBehavior.selectedScale = 1.2;
    
	[buttonLabel addBehavior:buttonBehavior];
	
	// observe button execute notification
	[self observeNotification:KKButtonDidExecuteNotification
					 selector:@selector(playButtonDidExecute:)
					   object:buttonLabel];
    
}

-(void) playButtonDidExecute:(NSNotification*)notification
{
    IntroScene *s = [[IntroScene alloc] initWithSize:self.size];
    SKTransition *doors = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:s transition:doors];
}


@end
