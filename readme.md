BubarSlasher
==============
a small iOS game developed in the frame of the Global Game Jam 2014.
[The page of the game on the Global Game Jam site](http://www.globalgamejam.org/2014/games/bubar-slasher)

Synopsis
--------
Mathias is a barber, but a very depressed one. Ashamed by his low hairiness, he bases all human relations around this criteria. Armed with his razor, he corrects people’s face to match his reality.

Platforms
---------
- iPad

Technology Notes 
-----------------
The game uses KoboldKit (and obviously SpriteKit).

Installation Instructions
---------------------------
- Open project with Xcode 5.x
- Run
- Enjoy

Credits 
--------
- Code :biou & Noliv
- Graphics : Noliv
- Music & sound design : Encelade
